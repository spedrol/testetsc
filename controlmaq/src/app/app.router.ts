import { Routes, CanActivate } from '@angular/router'
import { NgModule } from '@angular/core';
import { Injectable } from '@angular/core'
import {AppComponent} from './app.component'
import {HomeComponent} from './home/home.component'
import {MaquinaComponent} from './cadastros/maquina/maquina.component'
import {StatusmaqComponent} from './cadastros/statusmaq/statusmaq.component'

import {SimuladorComponent} from './cadastros/simulador/simulador.component'

export const ROUTER: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'maquina', component: MaquinaComponent},
  {path: 'status', component: StatusmaqComponent},
  {path: 'simulador', component: SimuladorComponent}
]
