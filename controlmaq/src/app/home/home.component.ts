import { Component, OnInit } from '@angular/core'
import { Maquina } from '../cadastros/maquina/maquina.model'
import { EventoStatus } from '../cadastros/maquina/eventostatus.model'
import { MaquinaService } from '../cadastros/maquina/maquina.service'
import { StatusService } from '../cadastros/statusmaq/statusmaq.service'
import { StatusMaq } from '../cadastros/statusmaq/statusmaq.model'

import swal from 'sweetalert2'
import { DatePipe } from '@angular/common';

@Component({
  selector: 'es-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  maquina: Maquina = new Maquina()
  listMaqs: Maquina[]=[]
  listStatus: StatusMaq[]=[]
  eventoStatus: EventoStatus[]=[]
  titulo: string = "STATUS: "

  intervalo: number = 100000//300000

  constructor(private maqService: MaquinaService,
    private stService: StatusService,
    private datePipe: DatePipe) { }

    ngOnInit() {
      this.getListarMaquinas()
      this.getListarStatus()
      window.setInterval(() => {
        window.location.reload();
      }, this.intervalo);
    }

    getListarMaquinas(){
      this.maqService.findAllMaq().subscribe(res => {
          this.listMaqs = res
      })
    }

    getListarStatus(){
      this.stService.findAllSt().subscribe(res => {
          this.listStatus = res
      })
    }

    recuperaMaq(maq: Maquina){
      this.titulo = "STATUS: " + maq.nomeMaquina
      this.eventoStatus = maq.eventoStatus
      this.maquina = maq
    }

    formataData(dt: Date) {
      let datePipe = new DatePipe("pt-BR")
      let dateObj = new Date(dt);
      var h = dateObj.getUTCHours()
      dateObj.setHours(h)
      var nData = datePipe.transform(dateObj, 'dd/MM/yyyy HH:mm:ss')
      return nData
  	}

    retornaEv(st: EventoStatus){
      let cod = ""
      for(let i = 0; i < this.listStatus.length; i++){
        if(this.listStatus[i].codigo == st.codStatus) {
          cod = this.listStatus[i].codigo +  " - " + this.listStatus[i].nomeStatus
        }
      }
      return cod
    }

    retornaEvSt(st: EventoStatus[]){
      let cod = "SEM STATUS"
      if(st.length > 0){
        for(let i = 0; i < this.listStatus.length; i++){
          if(this.listStatus[i].codigo == st[st.length - 1].codStatus){
            cod = this.listStatus[i].codigo +  " - " + this.listStatus[i].nomeStatus
          }
        }
      }
      return cod
    }

}
