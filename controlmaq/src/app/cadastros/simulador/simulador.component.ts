import { Component, OnInit } from '@angular/core';
import { Temporizador } from './simulador.model'
import { TemporizadorService } from  './simulador.service'
import swal from 'sweetalert2'

@Component({
  selector: 'es-simulador',
  templateUrl: './simulador.component.html',
  styleUrls: ['./simulador.component.css']
})
export class SimuladorComponent implements OnInit {

  temporizador: Temporizador = new Temporizador()
  segundos: string = "0"
  listSegundos: string[] = ["0", "1", "2", "3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"]
  minutos: string = "0"
  listMinutos: string[]=["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"]
  horas: string = "0"
  listHoras: string[]=["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23"]
  dias: string = "0"
  listDias: string[]=["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"]
  mes: string = "0"
  listMeses: string[]=["0","1","2","3","4","5","6","7","8","9","10","11","12"]
  liberado: boolean = false

  constructor(private tmpService: TemporizadorService) { }

  ngOnInit() {
    this.getListarTemp()
  }

  recuperaValor(tmp: string) {

    if(tmp != null){
      var array = tmp.split(' ');
      var temp = ""
      for(let i = 0; i < array.length; i++){
        if(array[i] != '*'){
          var list = array[i].split('/');
          if(i == 0){
            if(array[i] != '0'){
              temp = temp + list[1] + " Segundo(s)"
            }
          } else if(i == 1){
            if(array[i] != '0'){
              temp = temp + list[1] + " Minuto(s)"
            }
          } else if(i == 2){
            if(array[i] != '0'){
              temp = temp + list[1] + " Hora(s)"
            }
          } else if(i == 3){
            if(array[i] != '0'){
              temp = temp + list[1] + " Dia(s)"
            }
          } else if(i == 4){
            if(array[i] != '0'){
              temp = temp + list[1] + " Mês(es)"
            }
          } else if(i == 5){
            if(array[i] != '0'){
              temp = temp + list[1] + " Dia da semana"
            }
          }
        }
      }
      return temp
    } else {
      return ""
    }
  }

  getListarTemp (){
    this.tmpService.findTemp().subscribe(res => {
        if(res == null){
          this.temporizador.tempo = "0/5 * * * * *"
          this.tmpService.salvarTemp(this.temporizador).subscribe(res => {
            this.temporizador = res
          })
        } else {
          this.temporizador = res
        }
    })
  }

  startTemporizador() {
    this.tmpService.startTemp(this.temporizador).subscribe(res => {
      if(res != null && res > 0){
        swal("SUCESSO!", "Temporizador Iniciado com sucesso!", "success")
      }else{
        swal("Atenção!", "Erro ao Iniciar Temporizador!", "error")
      }
    })
  }

  stopTemporizador() {
    this.tmpService.stopTemp().subscribe(res => {
      if(res != null && res > 0){
        swal("SUCESSO!", "Temporizador Parado com sucesso!", "success")
      }else{
        swal("Atenção!", "Erro ao Parar Temporizador!", "error")
      }
    })
  }

  salvaTemporizador() {
    let m = " *"
    let d = " *"
    let h = " *"
    let mi = " *"
    let s = "*"
    let ms = parseInt(this.mes)
    let dia = parseInt(this.dias)
    let hor = parseInt(this.horas)
    let mn = parseInt(this.minutos)
    let sg = parseInt(this.segundos)

    if(ms > 0){
      m = " 0/" + this.mes
      d = " 0"
      h = " 0"
      mi = " 0"
      s = "0"
      this.liberado = true
    }

    if(dia > 0){
      d = " 0/" + this.dias
      h = " 0"
      mi = " 0"
      s = "0"
      this.liberado = true
    }

    if(hor > 0){
      h = " 0/" + this.horas
      mi = " 0"
      s = "0"
      this.liberado = true
    }

    if(mn > 0){
      mi = " 0/" + this.minutos
      s = "0"
      this.liberado = true
    }

    if(sg > 0){
      s = "0/" + this.segundos
      this.liberado = true
    }

    if(s == "*" && mi == " *" && h == " *" && d == " *" && m == " *"){
      swal("Atenção!", "É necessário escolher ao menos um valor para o temporizador!", "error")
    } else {
      this.temporizador.id = 1
      this.temporizador.tempo = s + mi + h + d + m + " *"
      this.tmpService.alterarTemp(this.temporizador).subscribe(res => {
        if(res != null && res > 0){
          swal("SUCESSO!", "Temporizador alterado com sucesso!", "success")
        }else{
          swal("Atenção!", "Erro ao alterar Temporizador!", "error")
        }
      })
    }
  }

}
