import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'
import { MAQ_API } from '../../app.api'
import { Temporizador } from './simulador.model'

@Injectable()
export class TemporizadorService {

  constructor(public http: HttpClient){}

  findTemp() : Observable<Temporizador> {
    return this.http.get<Temporizador>(`${MAQ_API}/temporizador`)
  }

  salvarTemp(st: Temporizador) : Observable<Temporizador>{
    return this.http.post<Temporizador>(`${MAQ_API}/temporizador`, st)
  }

  alterarTemp(st: Temporizador) : Observable<number>{
    return this.http.put<number>(`${MAQ_API}/temporizador`, st)
  }

  startTemp(st: Temporizador) : Observable<number>{
    return this.http.put<number>(`${MAQ_API}/atualizarevento/start`, st)
  }

  stopTemp() : Observable<number>{
    return this.http.get<number>(`${MAQ_API}/atualizarevento/stop`)
  }

  deletarTemp(st: Temporizador) : Observable<number>{
    return this.http.delete<number>(`${MAQ_API}/temporizador/${st.id}`)
  }

}
