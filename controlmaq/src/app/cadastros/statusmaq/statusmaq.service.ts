import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'
import { MAQ_API } from '../../app.api'
import { StatusMaq } from './statusmaq.model'

@Injectable()
export class StatusService {

  constructor(public http: HttpClient){}

  findAllSt() : Observable<StatusMaq[]> {
    return this.http.get<StatusMaq[]>(`${MAQ_API}/statusmaq`)
  }

  salvarSt(st: StatusMaq) : Observable<number>{
    return this.http.post<number>(`${MAQ_API}/statusmaq`, st)
  }

  alterarSt(st: StatusMaq) : Observable<number>{
    return this.http.put<number>(`${MAQ_API}/statusmaq`, st)
  }

  deletarSt(st: StatusMaq) : Observable<number>{
    return this.http.delete<number>(`${MAQ_API}/statusmaq/${st.id}`)
  }

}
