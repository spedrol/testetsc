import { Component, OnInit } from '@angular/core'
import { StatusMaq } from './statusmaq.model'
import { StatusService } from './statusmaq.service'
import swal from 'sweetalert2'

@Component({
  selector: 'es-statusmaq',
  templateUrl: './statusmaq.component.html',
  styleUrls: ['./statusmaq.component.css']
})
export class StatusmaqComponent implements OnInit {

  statusmaq: StatusMaq = new StatusMaq()
  listStatus: StatusMaq[]=[]
  titulo: string = "CADASTRO DE STATUS"

  constructor(private stService: StatusService) { }

  ngOnInit() {
    this.getListarStatus()
  }

  getListarStatus(){
    this.stService.findAllSt().subscribe(res => {
        this.listStatus = res
    })
  }

  limparCampos(){
      this.titulo = "CADASTRO DE STATUS"
      this.statusmaq = new StatusMaq()
      this.statusmaq.id = 0
  }

  recuperaStatus(st: StatusMaq){
    this.titulo = "ALTERAÇÃO DE STATUS"
    this.statusmaq = st

  }

  salvarStatus(){
    if(this.statusmaq.id != null && this.statusmaq.id != 0){
        this.stService.alterarSt(this.statusmaq).subscribe(res => {
          if(res != null && res > 0){
            swal("SUCESSO!", "Status alterado com sucesso!", "success")
            this.getListarStatus()
          }else{
            swal("Atenção!", "Erro ao alterar Status!", "error")
          }
        })
    }else{
      this.stService.salvarSt(this.statusmaq).subscribe(res => {
        if(res != null && res > 0){
          swal("SUCESSO!", "Status salvo com sucesso!", "success")
          this.getListarStatus()
        }else{
          swal("Atenção!", "Erro ao salvar Status!", "error")
        }
      })
    }
  }

  excluir(st: StatusMaq){
    swal({
      title: 'Tem Certeza que deseja apagar?',
      text: "Uma vez excluído, você não poderá recuperar este registro",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, Deletar!',
      cancelButtonText: 'Não'
    }).then((result) => {
      if (result.value) {
        this.stService.deletarSt(st).subscribe(res => {
          if(res != null && res > 0){
            swal("SUCESSO!", "Valor deletado com sucesso!", "success")
            this.getListarStatus()
            this.listStatus.splice(this.listStatus.indexOf(st), 1)
          }else{
            swal("Atenção!", "Erro ao deletar valor!", "error")
          }
        })
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'Cancelado',
          'Nenhum Registro foi apagado',
          'error'
        )
      }
    })
  }

}
