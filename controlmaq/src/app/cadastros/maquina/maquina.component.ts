import { Component, OnInit } from '@angular/core';
import { Maquina } from './maquina.model'
import { EventoStatus } from './eventostatus.model'
import { MaquinaService } from './maquina.service'

import swal from 'sweetalert2'

import { DatePipe } from '@angular/common';

@Component({
  selector: 'es-maquina',
  templateUrl: './maquina.component.html',
  styleUrls: ['./maquina.component.css']
})
export class MaquinaComponent implements OnInit {

  maquina: Maquina = new Maquina()
  listMaqs: Maquina[]=[]
  titulo: string = "CADASTRO DE MÁQUINAS"

  constructor(private maqService: MaquinaService,
    private datePipe: DatePipe) { }

  ngOnInit() {
    this.getListarMaquinas()
  }

  getListarMaquinas(){
    this.maqService.findAllMaq().subscribe(res => {
        this.listMaqs = res
    })
  }

  limparCampos(){
      this.titulo = "CADASTRO DE MÁQUINAS"
      this.maquina = new Maquina()
      this.maquina.id = 0
  }

  recuperaMaq(maq: Maquina){
    this.titulo = "ALTERAÇÃO DE MÁQUINAS"
    this.maquina = maq
    for (let i = 0; i < this.maquina.eventoStatus.length; i++) {
    //  this.maquina.eventoStatus[i].data = this.formataData(this.maquina.eventoStatus[i].data)
    }
  }

  formataData(dt: Date) {
    let datePipe = new DatePipe("pt-BR")
    let dateObj = new Date(dt);
    var h = dateObj.getUTCHours()
    dateObj.setHours(h)
    var nData = datePipe.transform(dateObj, 'yyyy-MM-dd HH:mm:ss')
    return nData
	}

  salvarMaquina(){
    if(this.maquina.id != null && this.maquina.id != 0){
        this.maqService.alterarMaq(this.maquina).subscribe(res => {
          if(res != null && res > 0){
            swal("SUCESSO!", "Máquina alterada com sucesso!", "success")
            this.getListarMaquinas()
          }else{
            swal("Atenção!", "Erro ao alterar máquina!", "error")
          }
        })
    }else{
      this.maqService.salvarMaq(this.maquina).subscribe(res => {
        if(res != null && res > 0){
          swal("SUCESSO!", "Máquina salva com sucesso!", "success")
          this.getListarMaquinas()
        }else{
          swal("Atenção!", "Erro ao salvar máquina!", "error")
        }
      })
    }
  }

  excluir(maq: Maquina){
    swal({
      title: 'Tem Certeza que deseja apagar?',
      text: "Uma vez excluído, você não poderá recuperar este registro",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, Deletar!',
      cancelButtonText: 'Não'
    }).then((result) => {
      if (result.value) {
        this.maqService.deletarMaq(maq).subscribe(res => {
          if(res != null && res > 0){
            swal("SUCESSO!", "Valor deletado com sucesso!", "success")
            this.getListarMaquinas()
            this.listMaqs.splice(this.listMaqs.indexOf(maq), 1)
          }else{
            swal("Atenção!", "Erro ao deletar valor!", "error")
          }
        })
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal(
          'Cancelado',
          'Nenhum Registro foi apagado',
          'error'
        )
      }
    })
  }

}
