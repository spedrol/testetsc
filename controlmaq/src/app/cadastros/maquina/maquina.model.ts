import {EventoStatus} from './eventostatus.model'

export class Maquina {

    id: number
    codMaq: string
  	nomeMaquina: string
  	eventoStatus: EventoStatus[]=[]

}
