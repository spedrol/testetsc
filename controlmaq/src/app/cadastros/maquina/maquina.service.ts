import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { MAQ_API } from '../../app.api';
import { Maquina } from './maquina.model'

@Injectable()
export class MaquinaService {

  constructor(public http: HttpClient){}

  findAllMaq() : Observable<Maquina[]> {
    return this.http.get<Maquina[]>(`${MAQ_API}/maquina`)
  }

  salvarMaq(maq: Maquina) : Observable<number>{
    return this.http.post<number>(`${MAQ_API}/maquina`, maq)
  }

  alterarMaq(maq: Maquina) : Observable<number>{
    return this.http.put<number>(`${MAQ_API}/maquina`, maq)
  }

  deletarMaq(maq: Maquina) : Observable<number>{
    return this.http.delete<number>(`${MAQ_API}/maquina/${maq.id}`)
  }

}
