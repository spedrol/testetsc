import { Component, OnInit } from '@angular/core'

declare var $:any;

@Component({
  selector: 'es-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

  isNotMobileMenu(){
      if($(window).width() > 991){
          return false;
      }
      return true;
  }

}
