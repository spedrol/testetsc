import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';

import { SidebarModule } from './sidebar/sidebar.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';

import { RouterModule, Routes } from '@angular/router';
import { ROUTER } from './app.router';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';

import {DatePipe, LocationStrategy, HashLocationStrategy} from '@angular/common';
import { MaquinaComponent } from './cadastros/maquina/maquina.component';
import { MaquinaService } from './cadastros/maquina/maquina.service';

import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { StatusmaqComponent } from './cadastros/statusmaq/statusmaq.component';
import { StatusService } from './cadastros/statusmaq/statusmaq.service';
import { SimuladorComponent } from './cadastros/simulador/simulador.component';

import { TemporizadorService } from './cadastros/simulador/simulador.service';

registerLocaleData(localePt);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    FooterComponent,
    MaquinaComponent,
    StatusmaqComponent,
    SimuladorComponent
  ],
  imports: [
    RouterModule.forRoot(ROUTER),
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    SidebarModule
  ],
  providers: [
    MaquinaService,
    StatusService,
    TemporizadorService,
    DatePipe,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
