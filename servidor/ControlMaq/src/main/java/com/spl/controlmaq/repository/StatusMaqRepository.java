package com.spl.controlmaq.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spl.controlmaq.model.StatusMaq;

@Repository
public interface StatusMaqRepository extends JpaRepository<StatusMaq, Integer> {

	@Transactional(readOnly=true)
	@Query("select s from StatusMaq s where s.id = :id")
	StatusMaq statusMaqById(@Param("id") Integer id);
	
}
