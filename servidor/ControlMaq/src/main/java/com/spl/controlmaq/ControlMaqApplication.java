package com.spl.controlmaq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControlMaqApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControlMaqApplication.class, args);
	}

}
