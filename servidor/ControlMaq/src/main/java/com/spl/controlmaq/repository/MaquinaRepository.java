package com.spl.controlmaq.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spl.controlmaq.model.Maquina;

@Repository
public interface MaquinaRepository extends JpaRepository<Maquina, Integer> {

	@Transactional(readOnly=true)
	@Query("select m from Maquina m where m.id = :id")
	Maquina maquinaById(@Param("id") Integer id);
	
}
