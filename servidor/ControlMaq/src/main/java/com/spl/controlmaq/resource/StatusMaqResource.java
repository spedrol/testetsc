package com.spl.controlmaq.resource;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.spl.controlmaq.model.StatusMaq;
import com.spl.controlmaq.repository.StatusMaqRepository;
import com.spl.controlmaq.service.StatusMaqService;

@RestController
@RequestMapping(value = "/statusmaq")
public class StatusMaqResource {

	@Autowired
	private StatusMaqService stmService;
	
	@Autowired
	private StatusMaqRepository stmRepo;
		
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Integer> insert(@RequestBody StatusMaq maq) {
		StatusMaq obj = stmService.insert(maq);
		return new ResponseEntity<Integer>(obj.getId(), HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Integer> update(@RequestBody StatusMaq maq) {
		StatusMaq obj = stmService.update(maq);
		return new ResponseEntity<Integer>(obj.getId(), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<StatusMaq>> find() {
		List<StatusMaq> maqs = stmService.findAll();
		return ResponseEntity.ok().body(maqs);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/{id}")
	public ResponseEntity<Integer> delete(@PathVariable Integer id) {
		StatusMaq maq = stmRepo.statusMaqById(id);
		if(maq == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		stmService.remove(maq);
		return new ResponseEntity<Integer>(maq.getId(), HttpStatus.OK);
	}
}
