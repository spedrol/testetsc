package com.spl.controlmaq.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spl.controlmaq.model.StatusMaq;
import com.spl.controlmaq.repository.StatusMaqRepository;

@Service
public class StatusMaqService {
	
	@Autowired
	private StatusMaqRepository repo;
	
	@PersistenceContext
    private EntityManager em;

	public StatusMaq insert(StatusMaq obj) {
		obj.setId(0);
		return repo.save(obj);
	}
	
	public StatusMaq update(StatusMaq obj) {
		return repo.save(obj);
	}
	
	public void remove(StatusMaq user) {
		 repo.delete(user);
	}
	
	public List<StatusMaq> findAll() {
		return repo.findAll();
	}
}
