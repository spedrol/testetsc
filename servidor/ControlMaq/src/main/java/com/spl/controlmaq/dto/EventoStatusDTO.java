package com.spl.controlmaq.dto;

import java.io.Serializable;
import java.util.Date;

public class EventoStatusDTO  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int id;
	private int codStatus;
    private Date data;
	
	public EventoStatusDTO() {}
	
	public EventoStatusDTO(int id, int codStatus, Date data) {
		super();
		this.id = id;
		this.codStatus = codStatus;
		this.data = data;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCodStatus() {
		return codStatus;
	}

	public void setCodStatus(int codStatus) {
		this.codStatus = codStatus;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
}
