package com.spl.controlmaq.dto;

import java.io.Serializable;
import java.util.List;

public class MaquinaDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String codMaq;
	private String nomeMaquina;
	
    private List<EventoStatusDTO> eventoStatus;
	
	public MaquinaDTO() {}

	public MaquinaDTO(int id, String codMaq, String nomeMaquina, List<EventoStatusDTO> eventoStatus) {
		super();
		this.id = id;
		this.codMaq = codMaq;
		this.nomeMaquina = nomeMaquina;
		this.eventoStatus = eventoStatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodMaq() {
		return codMaq;
	}

	public void setCodMaq(String codMaq) {
		this.codMaq = codMaq;
	}

	public String getNomeMaquina() {
		return nomeMaquina;
	}

	public void setNomeMaquina(String nomeMaquina) {
		this.nomeMaquina = nomeMaquina;
	}

	public List<EventoStatusDTO> getEventoStatus() {
		return eventoStatus;
	}

	public void setEventoStatus(List<EventoStatusDTO> eventoStatus) {
		this.eventoStatus = eventoStatus;
	}
}
