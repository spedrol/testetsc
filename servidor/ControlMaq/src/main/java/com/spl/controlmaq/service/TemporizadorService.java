package com.spl.controlmaq.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spl.controlmaq.model.Temporizador;
import com.spl.controlmaq.repository.TemporizadorRepository;

@Service
public class TemporizadorService {
	
	@Autowired
	private TemporizadorRepository repo;
	
	@PersistenceContext
    private EntityManager em;

	public Temporizador insert(Temporizador obj) {
		obj.setId(0);
		return repo.save(obj);
	}
	
	public Temporizador update(Temporizador obj) {
		return repo.save(obj);
	}
	
	public void remove(Temporizador user) {
		 repo.delete(user);
	}
	
	public List<Temporizador> findAll() {
		return repo.findAll();
	}
}
