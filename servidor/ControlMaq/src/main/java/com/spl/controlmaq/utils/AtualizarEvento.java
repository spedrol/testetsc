package com.spl.controlmaq.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ScheduledFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.spl.controlmaq.model.EventoStatus;
import com.spl.controlmaq.model.Maquina;
import com.spl.controlmaq.model.StatusMaq;
import com.spl.controlmaq.model.Temporizador;
import com.spl.controlmaq.service.MaquinaService;
import com.spl.controlmaq.service.StatusMaqService;

@RestController
@RequestMapping("/atualizarevento")
public class AtualizarEvento {

    public String cron  = "0/5 * * * * *";
    private List<Maquina> listMaq;
    private List<StatusMaq> listStatus;
    private List<Integer> listCodStatus;
    
    @Autowired
	private MaquinaService maqService;
    
    @Autowired
	private StatusMaqService stmService;
    
    @Autowired
    private TaskScheduler task;
    
    private ScheduledFuture<?> scheduledFuture;

    @RequestMapping(method = RequestMethod.PUT, value="start")
	public ResponseEntity<Integer> start(@RequestBody Temporizador temp) {   
    	this.listMaq = new ArrayList<>();
    	this.listStatus = new ArrayList<>();

    	this.listCodStatus = new ArrayList<>();
    	this.listMaq = maqService.findAll();
    	this.listStatus = stmService.findAll();    
    	
    	for(StatusMaq s : listStatus) {
    		this.listCodStatus.add(s.getCodigo());
    	}
    	    	
        scheduledFuture = task.schedule(printHour(), new CronTrigger(temp.getTempo()));
        return ResponseEntity.ok().body(1);
    }
    
    @RequestMapping(method = RequestMethod.GET, value="stop")
	public ResponseEntity<Integer> stop() {
        scheduledFuture.cancel(false);
        return ResponseEntity.ok().body(1);
    }

    private Runnable printHour() {
        return () -> eventomaquina();
    }
    
    private void eventomaquina() {
    	try {    		       	
        	Date d = new Date();
        	Calendar cal = new GregorianCalendar();
        	cal.setTime(d);
        	cal.add(Calendar.HOUR_OF_DAY, -3);
        	System.out.println(cal.getTime());
        	if(listCodStatus.size() > 0 && this.listMaq.size() > 0) {
    	    	for(Maquina m : this.listMaq) {
    	    		Collections.shuffle(this.listCodStatus);
    	    		List<EventoStatus> listEv = new ArrayList<>();
    	    		listEv.add(new EventoStatus(0, this.listCodStatus.get(0), cal.getTime(), m));
    	    		m.setEventoStatus(listEv);
    	    		maqService.update(m);
    	    	}
        	}        	
		} catch (Exception e) {
			System.out.println("error -> AtualizaEvento: eventomaquina() " + e);
		}
    }
}
