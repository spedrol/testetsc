package com.spl.controlmaq.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spl.controlmaq.model.Temporizador;

@Repository
public interface TemporizadorRepository extends JpaRepository<Temporizador, Integer> {

	@Transactional(readOnly=true)
	@Query("select t from Temporizador t where t.id = :id")
	Temporizador tempById(@Param("id") Integer id);
	
}
