package com.spl.controlmaq.resource;

import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.spl.controlmaq.model.Temporizador;
import com.spl.controlmaq.repository.TemporizadorRepository;
import com.spl.controlmaq.service.TemporizadorService;

@RestController
@RequestMapping(value = "/temporizador")
public class TemporizadorResource {

	@Autowired
	private TemporizadorService tmpService;
	
	@Autowired
	private TemporizadorRepository tmpRepo;
		
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Temporizador> insert(@RequestBody Temporizador tmp) {
		Temporizador obj = tmpService.insert(tmp);
		return new ResponseEntity<Temporizador>(obj, HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Integer> update(@RequestBody Temporizador tmp) {
		Temporizador obj = tmpService.update(tmp);
		return new ResponseEntity<Integer>(obj.getId(), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Temporizador> find() {
		Temporizador tmp = tmpRepo.tempById(1);
		return ResponseEntity.ok().body(tmp);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/{id}")
	public ResponseEntity<Integer> delete(@PathVariable Integer id) {
		Temporizador tmp = tmpRepo.tempById(id);
		if(tmp == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		tmpService.remove(tmp);
		return new ResponseEntity<Integer>(tmp.getId(), HttpStatus.OK);
	}
}
