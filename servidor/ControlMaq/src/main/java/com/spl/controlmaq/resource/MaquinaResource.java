package com.spl.controlmaq.resource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.spl.controlmaq.dto.EventoStatusDTO;
import com.spl.controlmaq.dto.MaquinaDTO;
import com.spl.controlmaq.model.EventoStatus;
import com.spl.controlmaq.model.Maquina;
import com.spl.controlmaq.repository.MaquinaRepository;
import com.spl.controlmaq.service.MaquinaService;

@RestController
@RequestMapping(value = "/maquina")
public class MaquinaResource {

	@Autowired
	private MaquinaService maqService;
	
	@Autowired
	private MaquinaRepository maqRepo;
		
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Integer> insert(@RequestBody Maquina maq) {
		Maquina obj = maqService.insert(maq);
		return new ResponseEntity<Integer>(obj.getId(), HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Integer> update(@RequestBody Maquina maq) {		
		List<EventoStatus> lstEv = new ArrayList<>();
		for(EventoStatus ev: maq.getEventoStatus()) {
			ev.setMaquina(maq);
			lstEv.add(ev);
		}
		maq.setEventoStatus(lstEv);
		Maquina obj = maqService.update(maq);
		return new ResponseEntity<Integer>(obj.getId(), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<MaquinaDTO>> find() {
		List<Maquina> maqs = maqService.findAll();
		List<MaquinaDTO> maqsDTO = new ArrayList<>();
		for(Maquina c: maqs) {
			maqsDTO.add(retornaMaqDTO(c));
		}
		return ResponseEntity.ok().body(maqsDTO);
	}
	
	public MaquinaDTO retornaMaqDTO(Maquina maq) {
		List<EventoStatusDTO> evs = new ArrayList<>();
		for(EventoStatus e: maq.getEventoStatus()) {
			evs.add(new EventoStatusDTO(e.getId(), e.getCodStatus(), e.getData()));
		}		
		MaquinaDTO maqDTO  = new MaquinaDTO(maq.getId(),maq.getCodMaq(), maq.getNomeMaquina(), evs);		
		return maqDTO;
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/{id}")
	public ResponseEntity<Integer> delete(@PathVariable Integer id) {
		Maquina maq = maqRepo.maquinaById(id);
		if(maq == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		maqService.remove(maq);
		return new ResponseEntity<Integer>(maq.getId(), HttpStatus.OK);
	}
}
