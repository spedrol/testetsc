package com.spl.controlmaq.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spl.controlmaq.model.Maquina;
import com.spl.controlmaq.repository.MaquinaRepository;

@Service
public class MaquinaService {
	
	@Autowired
	private MaquinaRepository repo;
	
	@PersistenceContext
    private EntityManager em;

	public Maquina insert(Maquina obj) {
		obj.setId(0);
		return repo.save(obj);
	}
	
	public Maquina update(Maquina obj) {
		return repo.save(obj);
	}
	
	public void remove(Maquina user) {
		 repo.delete(user);
	}
	
	public List<Maquina> findAll() {
		return repo.findAll();
	}
}
