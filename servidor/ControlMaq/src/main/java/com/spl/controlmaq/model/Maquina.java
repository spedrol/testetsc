package com.spl.controlmaq.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "maquina")
public class Maquina implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String codMaq;
	private String nomeMaquina;
	
	@OneToMany(mappedBy="maquina", cascade=CascadeType.ALL)
    private List<EventoStatus> eventoStatus;
	
	public Maquina() {}

	public Maquina(int id, String codMaq, String nomeMaquina, List<EventoStatus> eventoStatus) {
		super();
		this.id = id;
		this.codMaq = codMaq;
		this.nomeMaquina = nomeMaquina;
		this.eventoStatus = eventoStatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodMaq() {
		return codMaq;
	}

	public void setCodMaq(String codMaq) {
		this.codMaq = codMaq;
	}

	public String getNomeMaquina() {
		return nomeMaquina;
	}

	public void setNomeMaquina(String nomeMaquina) {
		this.nomeMaquina = nomeMaquina;
	}

	public List<EventoStatus> getEventoStatus() {
		return eventoStatus;
	}

	public void setEventoStatus(List<EventoStatus> eventoStatus) {
		this.eventoStatus = eventoStatus;
	}
}
